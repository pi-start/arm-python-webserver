# Alpine Python Image
FROM arm32v7/python:3.7.6-alpine
# bring in QEMU binaries
COPY qemu-arm-static /usr/bin/qemu-arm-static
COPY index.html /
COPY simple-server.py /app/
EXPOSE 8080
CMD ["python3", "/app/simple-server.py"]
